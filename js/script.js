// Back to top button
$(document).ready(function(){
    //Back to the top button
    $('.backToTop').click(function(){
        $('html,body').animate({ scrollTop: 0 }, 'slow');
    });
});

//Sticks the title to the top od the window
function sticky_relocate() {
    var window_top = $(window).scrollTop();
    var div_top = $('.sticky-anchor').offset().top;
    if (window_top > div_top) {
        $('.slider_content').addClass('stick');
        $('.sticky-anchor').height($('.sticky').outerHeight());
    } 
    else {
        $('.slider_content').removeClass('stick');
        $('.sticky-anchor').height(0);
    } 
}

$(function() {
    $(window).scroll(sticky_relocate);
    sticky_relocate();
});

// An AJAX-jQuery function preventing a redirect to php script file
$(function() {
    $("#submit").click(function() {
        var data = {
            email: $("#email").val(),
            message: $("#message").val()
        };
        $.ajax({
            type: "POST",
            url: "../php/mailer.php",
            data: data,
            success: function(){
                $('.success').addClass('visible');
            }
        });
        return false;

    });

});